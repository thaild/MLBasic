# MLBasic
Research machinelearning

This repository using jupyter notebook to take note 

# Setup instruction

(Installation jupyter)[https://jupyter.org/install.html]
------------

JupyterLab can be installed using ``conda``, ``pip``, ``pipenv`` or ``docker``.

### conda


If you use ``conda``, you can install it with:

``` bash
conda install -c conda-forge jupyterlab
```

### pip


If you use ``pip``, you can install it with:

```bash
pip install jupyterlab
```

If installing using ``pip install --user``, you must add the user-level
``bin`` directory to your ``PATH`` environment variable in order to launch
``jupyter lab``.

# Serve (local run) instruction

・at root directory run:

```
jupyter notebook
```
